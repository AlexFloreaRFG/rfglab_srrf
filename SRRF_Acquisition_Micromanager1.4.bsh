//SCRIPT TO ACQUIRE TIMELAPSE MULTI-Z SRRF IMAGES
//Works on Micromanager nightly builds after November 20th, 2018

//Import Required libraries
import org.micromanager.api.AcquisitionOptions;
import ij.IJ;
import ij.gui.*;
import java.awt.*;
import java.lang.System;
import ij.text.TextWindow;
import ij.text.TextPanel;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

// CHANGE ONLY CODE BELOW THIS LINE
///////////////////////////////////////////////////////////////

// Set the acquisition name
acqName = gui.getUniqueAcquisitionName("25nmBeads");

// Select the root directory to save to
rootDirName = "D:/fernandezgonzalez/Alex/20190813";

// Number of timepoints
nrTimepoints = 1;
// Number of slices
nrSlices = 10;
// Step Size between slices (in um)
stepSize = 0.02;
// Time between time points (in ms)
intervalMs = 10000;
// Images per SRRF burst (taken for each slice)
imagesPerBurst = 100;
// Set exposure (in ms)
exposure = 20;

// DO NOT CHANGE CODE BELOW THIS LINE
///////////////////////////////////////////////////////////////

// Create a new folder in the specified directory
// Will automatically increment folder name

counter = 0;
filepath = rootDirName  +  "/" + acqName + counter;

File dir = new File(filepath);
while(dir.exists()) {
	counter = counter + 1;
	filepath = rootDirName  + "/" + acqName + counter;
	dir = new File(filepath);
}
new File(rootDirName + "/" + acqName + counter).mkdirs();
rootDirName = rootDirName + "/" + acqName + counter + "/";

FileWriter output = new FileWriter(rootDirName + "AcquisitionParameters.txt");
BufferedWriter out = new BufferedWriter(output);
out.write("nrTimepoints = " + nrTimepoints + "\r\n");
out.write("nrSlices = " + nrSlices + "\r\n");
out.write("stepSize = " + stepSize + "\r\n");
out.write("intervalMs = " + intervalMs + "\r\n");
out.write("imagesPerBurst = " + imagesPerBurst + "\r\n");
out.write("exposure = " + exposure + "\r\n");

out.flush();
out.close();

//Clear all acquisitions
gui.closeAllAcquisitions();
gui.clearMessageWindow();

//Set exposure
mmc.setExposure(exposure);

//Get initial Z position
topPos = mmc.getPosition();

//Loop for required number of timepoints
for(currentTimepoint=0; currentTimepoint < nrTimepoints; currentTimepoint++){
	//Track time at which we started the frame
	timeStartCurrentFrame = System.currentTimeMillis();

	//Start an acquisition for each frame
	gui.closeAllAcquisitions();
	gui.openAcquisition("time" + currentTimepoint, rootDirName, nrSlices, 1, imagesPerBurst, 1, /* show */ true, /* save */ true);
		
	//Clear the circular buffer
	mmc.clearCircularBuffer();

	//Go through each slice of a Z-Stack
	for(currentSlice=0; currentSlice < nrSlices; currentSlice++) {
		
		//Start sequence acquisition with the total number of images you need to take for slice
		mmc.startSequenceAcquisition(imagesPerBurst, 0, true);
		//Initialize a variable to keep track of how many more images we need to take for this slice
		remaining = imagesPerBurst;

		//Pops an image until we have none left at this spot		
		while (remaining > 0 || mmc.deviceBusy(mmc.getCameraDevice())) {

			//Add images to the acquisition, and update how many are left
			if (mmc.getRemainingImageCount() > 0) {
				img = mmc.popNextTaggedImage();
				gui.addImageToAcquisition("time" + currentTimepoint, currentSlice, 0, (imagesPerBurst)-remaining, 0, img);
				remaining--;
		   }
		   //Otherwise, we wait for an image to be ready
		   else {
		     mmc.sleep(Math.min(0.5*exposure, 20));
		   }
		}  

		//Once the slice has been acquired, stop the acquisition so we can save, and clear the circular buffer
		mmc.stopSequenceAcquisition();
		mmc.clearCircularBuffer();
		
		//Once we've acquired enough burst images at that slice, move Z position by another step 
		mmc.setPosition(mmc.getFocusDevice(), topPos - (currentSlice+1)*stepSize);

	}


	//Print the time it took to acquire all the images
   acqTime = System.currentTimeMillis() - timeStartCurrentFrame;
	acqTimeString = acqTime.toString();

	//Close the acquisition
 	gui.closeAcquisitionWindow("time" + currentTimepoint);
	
	//Now that we've acquired the entire stack, we check how much time is left before the next frame and move the stage back to the top
	mmc.setPosition(mmc.getFocusDevice(), topPos);
   totalTime = System.currentTimeMillis() - timeStartCurrentFrame;
	gui.message("Acquire Time: " + acqTimeString);

	//If there is time left, we wait the rest before starting next frame
   if ((intervalMs - totalTime) > 0) {
		gui.sleep(intervalMs - totalTime);
   }

}
