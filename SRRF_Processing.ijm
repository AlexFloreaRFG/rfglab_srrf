//This Macro opens the raw images and processes them into SRRF images.
//It automatically creates and saves a new folder for SRRF and the Average Intensity Projection of the raw data

//Change code below this line
////////////////////////////////////////////////////////
//Root directory containing the raw data
dir = "C:/Users/AlexFlorea/Documents/SRRF/SRRF Data/20190813/25nmBeads5";

//Number of slices in the Z Stack
nrSlices = 3;
//Number of images to be converted into a SRRF image
nrImagesPerSlice = 100;

//Set the SRRF parameters below
ringSize = 0.5; // 0.1 to 3
radMag = 5; // 1 to 10
axes = 8; // 2 to 8

// Do not change code below this line
////////////////////////////////////////////////////////
print("Source Data = " + dir);
print("nrSlices = " + nrSlices);
print("nrImagesPerSlice = " + nrImagesPerSlice);
print("ringSize = " + ringSize);
print("radMag = " + radMag);
print("axes = " + axes);


//Get the number of timepoints in the folder
list = getFileList(dir);
nrTimepoints = list.length-1;

// Make directories to save final images
SRRFdir = dir + "/SRRF/";
Avdir = dir + "/Average/";

File.makeDirectory(SRRFdir);
File.makeDirectory(Avdir);


selectWindow("Log");  //select Log-window 
saveAs("Text", SRRFdir + "SRRF Parameters"); 


start = getTime();

//Open each timepoint one by one
for(timepoint = 0; timepoint < nrTimepoints; timepoint++){

	//Make a directory to save each timepoint
	File.makeDirectory(SRRFdir + "time" + timepoint + "/");

	
	//Make a directory to save each timepoint for the average of the image
	File.makeDirectory(Avdir + "time" + timepoint + "/");

	//Open all the images of one timepoint
	run("Image Sequence...", "open=[" + dir + "/time" + timepoint + "/Pos0/img_000000000_Default0_001.tif] sort");
	
	//Take the entire stack and split it into substacks that contain each of the slices
	for(slice=0; slice < nrSlices; slice++){

		//Calculate the first and last index of the slice
		first = 1+nrImagesPerSlice*slice;
		last = nrImagesPerSlice*(slice+1);

		//Make and save the substack
		run("Make Substack..."	, " slices=" + first + "-" + last);

		//Run NanoJ-SRRF on the substack and save it in the SRRF Directory
		
		run("SRRF Analysis", "ring="+ringSize+" radiality_magnification=" +radMag+" axes="+axes+" frames_per_time-point=0 start=0 end=0 max=100 preferred=0");
		saveAs("Tiff", SRRFdir + "time" + timepoint + "/slice" + slice +".tif");

		//Close SRRF image and raw image stack
		close();

		//Run Average Projection on the substack and save it in the Average Directory
		run("Z Project...", "projection=[Average Intensity]");
		saveAs("Tiff", Avdir + "time" + timepoint + "/slice" + slice +".tif");
		close();
		close();
	}

	//Now that we finished making substacks, close the timepoint
	close();
}

//Now we need to concatenate each frame into an individual stack
//Add a directory to save each frame in
File.makeDirectory(SRRFdir + "AllTimepoints/");


for(timepoint = 0; timepoint < nrTimepoints; timepoint++){

	//Open all the images  and save as a stack
	run("Image Sequence...", "open=[" + SRRFdir + "time" + timepoint + "/slice0.tif] sort");
	saveAs("Tiff", SRRFdir + "AllTimepoints/" + "n1_t" + timepoint + ".TIF");

	//Close SRRF Stack
	close();
}

//And do the same for the average intensity reconstruction
File.makeDirectory(Avdir + "AllTimepoints/");
//Open each timepoint one by one
for(timepoint = 0; timepoint < nrTimepoints; timepoint++){

	//Open all the images  and save as a stack
	run("Image Sequence...", "open=[" + Avdir + "time" + timepoint +"/slice0.tif] sort");
	saveAs("Tiff", Avdir + "AllTimepoints/" + "time" + timepoint + ".TIF");

	//Close SRRF Stack
	close();
}

itTook = getTime() - start;
print(itTook)
